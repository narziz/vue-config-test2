const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { merge } = require("webpack-merge");

const common = require("../webpack.common");

module.exports = merge(common, {
  mode: "development",
  entry: {
    main: {
      import: "./src/main.js",
      filename: "js/main.js",
    },
    emas: {
      import: "./src/emas/main.js",
      filename: "projects/emas/js/emas.js",
    },
    mas: {
      import: "./src/mas/main.js",
      filename: "projects/mas/js/mas.js",
    },
  },
  output: {
    filename: "./[name]",
    path: path.resolve(__dirname, "../dist"),
    assetModuleFilename: "projects/assets/images/[name][ext][query]",
    clean: true,
  },
  plugins: [
    new HtmlWebpackPlugin({
      name: "Main App",
      filename: "index.html",
      template: "./public/index.html",
      inject: "body",
      // chunks: ['main']
    }),
    new HtmlWebpackPlugin({
      name: "Emas App",
      filename: "projects/emas/index.html",
      template: "./public/emas.html",
      inject: "body",
      chunks: ["emas"],
    }),
    new HtmlWebpackPlugin({
      name: "Mas App",
      filename: "projects/mas/index.html",
      template: "./public/mas.html",
      inject: "body",
      chunks: ["mas"],
    }),
  ],
  devServer: {
    static: {
      directory: path.join(__dirname, "../dist"),
    },
    port: 4000,
  },
});
