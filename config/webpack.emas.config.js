const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { merge } = require('webpack-merge');

const common = require('../webpack.common');

module.exports = merge(common, {
  mode: 'development',
  entry: {
    emas: './src/emas/main.js'
  },
  output: {
    filename: './js/[name].js',
    path: path.resolve(__dirname, '../dist/projects/emas'),
    assetModuleFilename: '../assets/images/[name][ext][query]',
  },
  plugins: [
    new HtmlWebpackPlugin({
      name: 'Emas App',
      filename: 'index.html',
      template: './public/emas.html',
      inject: 'body'
    })
  ],
  devServer: {
    static: {
      directory: path.join(__dirname, '../dist/projects/emas'),
    },
    port: 4000,
  },
});