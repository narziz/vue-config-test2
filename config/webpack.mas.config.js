const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { merge } = require('webpack-merge');

const common = require('../webpack.common');

module.exports = merge(common, {
  mode: 'development',
  entry: {
    index: './src/mas/main.js'
  },
  output: {
    filename: './js/main.js',
    path: path.resolve(__dirname, '../dist/projects/mas'),
    assetModuleFilename: '../assets/images/[name][ext][query]',
  },
  plugins: [
    new HtmlWebpackPlugin({
      name: 'Mas App',
      filename: 'index.html',
      template: './public/mas.html',
      inject: 'body'
    })
  ],
  devServer: {
    static: {
      directory: path.join(__dirname, '../dist/projects/mas'),
    },
    port: 4000,
  },
});